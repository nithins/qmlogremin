/*!
 * \copyright (c) Nokia Corporation and/or its subsidiary(-ies) (qt-info@nokia.com) and/or contributors
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * \license{This source file is part of QmlOgre abd subject to the BSD license that is bundled
 * with this source code in the file LICENSE.}
 */

#ifndef OGREITEM_H
#define OGREITEM_H

#include <Ogre.h>
#include <QQuickWindow>

#include <QtQuick/QQuickItem>
#include <QtQuick/QSGGeometry>
#include <QtQuick/QSGTextureMaterial>
#include <QtCore/QPropertyAnimation>



class OgreItem : public QQuickItem,public Ogre::FrameListener,public Ogre::RenderTargetListener
{
  Q_OBJECT

  Q_PROPERTY(qreal yaw READ yaw WRITE setYaw)
  Q_PROPERTY(qreal pitch READ pitch WRITE setPitch)
  Q_PROPERTY(qreal zoom READ zoom WRITE setZoom)
public:
  qreal yaw() const       { return m_yaw; }
  qreal pitch() const     { return m_pitch; }
  qreal zoom() const      { return m_zoom; }
  void setYaw(qreal y)    { m_yaw = y; }
  void setPitch(qreal p)  { m_pitch = p; }
  void setZoom(qreal z)   { m_zoom = z; }

  OgreItem(QQuickItem *parent = 0);
  virtual ~OgreItem();

protected:
  virtual QSGNode *updatePaintNode(QSGNode *, UpdatePaintNodeData *);

  virtual bool frameStarted(const Ogre::FrameEvent& evt);

  virtual void preRenderTargetUpdate(const Ogre::RenderTargetEvent& evt);
  virtual void postRenderTargetUpdate(const Ogre::RenderTargetEvent& evt);


  void updateFBO();
  void initCamera();
  void updateCamera();

private:

  QSGTextureMaterial       m_material;
  QSGOpaqueTextureMaterial m_materialO;
  QSGGeometry              m_geometry;
  QSize                    m_size;
  QSGTexture              *m_texture;
  Ogre::RenderTexture     *m_renderTarget;

  Ogre::Camera        *m_camera;
  Ogre::SceneNode     *m_camera_node;
  qreal                m_yaw;
  qreal                m_pitch;
  qreal                m_zoom;


};

#endif // OGREITEM_H
