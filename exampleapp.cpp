/*!
 * \copyright (c) Nokia Corporation and/or its subsidiary(-ies) (qt-info@nokia.com) and/or contributors
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * \license{This source file is part of QmlOgre abd subject to the BSD license that is bundled
 * with this source code in the file LICENSE.}
 */

#include "exampleapp.h"

#include "ogreitem.h"

#include <QCoreApplication>
#include <QtQml/QQmlContext>
#include <QDir>
#include <QOpenGLContext>
#include <QOpenGLFunctions>


ExampleApp::ExampleApp(QWindow *parent) :
  QQuickView(parent)
, m_sceneManager(0)
, m_root(0)
, m_ogreContext(0)
, m_qtContext(0)
{
  // start Ogre once we are in the rendering thread (Ogre must live in the rendering thread)
  connect(this, &ExampleApp::beforeRendering, this, &ExampleApp::initializeOgre, Qt::DirectConnection);
  connect(this, &ExampleApp::ogreInitialized, this, &ExampleApp::addContent);

  startTimer(16);
}

ExampleApp::~ExampleApp()
{
  if (m_sceneManager) {
    m_root->destroySceneManager(m_sceneManager);
  }
  if(m_ogreContext)
    delete m_ogreContext;
}

void ExampleApp::initializeOgre()
{
  // we only want to initialize once
  disconnect(this, &ExampleApp::beforeRendering, this, &ExampleApp::initializeOgre);


  // Setup the shared opengl context.
  m_qtContext   = QOpenGLContext::currentContext();
  m_ogreContext = new QOpenGLContext();
  m_ogreContext->setFormat(this->requestedFormat());
  m_ogreContext->setShareContext(m_qtContext);
  m_ogreContext->create();

  // Initialize the ogre render system. The ogre context must be activated first
  activateOgreContext();


  Ogre::ConfigFile cf;
#ifdef _MSC_VER
  m_root = new Ogre::Root(Ogre::String("plugins" OGRE_BUILD_SUFFIX ".cfg"));
  cf.load(Ogre::String("resources" OGRE_BUILD_SUFFIX ".cfg"));
#else
  m_root = new Ogre::Root(Ogre::String("plugins.cfg"));
  cf.load(Ogre::String("resources.cfg"));
#endif

  Ogre::RenderSystem *renderSystem = m_root->getRenderSystemByName("OpenGL Rendering Subsystem");
  m_root->setRenderSystem(renderSystem);
  m_root->initialise(false);

  Ogre::NameValuePairList params;

  params["externalGLControl"] = "true";
  params["currentGLContext"] = "true";

#if defined(Q_OS_MAC) || defined(Q_OS_WIN)
  params["externalWindowHandle"] = Ogre::StringConverter::toString((size_t)(this->winId()));
  params["parentWindowHandle"] = Ogre::StringConverter::toString((size_t)(this->winId()));
#else
  params["externalWindowHandle"] = Ogre::StringConverter::toString((unsigned long)(this->winId()));
  params["parentWindowHandle"] = Ogre::StringConverter::toString((unsigned long)(this->winId()));
#endif


  //Finally create our window.
  Ogre::RenderWindow *  ogreWindow = m_root->
      createRenderWindow("OgreWindow", 1, 1, false, &params);
  ogreWindow->setVisible(false);
  ogreWindow->update(false);

  // Go through all sections & settings in the file
  Ogre::ConfigFile::SectionIterator seci = cf.getSectionIterator();

  Ogre::String secName, typeName, archName;
  while (seci.hasMoreElements())
  {
    secName = seci.peekNextKey();
    Ogre::ConfigFile::SettingsMultiMap *settings = seci.getNext();
    Ogre::ConfigFile::SettingsMultiMap::iterator i;
    for (i = settings->begin(); i != settings->end(); ++i)
    {
      typeName = i->first;
      archName = i->second;

      Ogre::ResourceGroupManager::getSingleton().addResourceLocation(
            archName, typeName, secName);
    }
  }

  Ogre::ResourceGroupManager::getSingleton().initialiseAllResourceGroups();


  // set up Ogre scene
  m_sceneManager = m_root->createSceneManager(Ogre::ST_GENERIC, "mySceneManager");

  m_sceneManager->setAmbientLight(Ogre::ColourValue(0.3, 0.3, 0.3));
  m_sceneManager->createLight("myLight")->setPosition(20, 80, 50);

  m_sceneManager->setSkyBox(true, "SpaceSkyBox", 10000);
  m_sceneManager->getRootSceneNode()->attachObject(m_sceneManager->createEntity("Head", "ogrehead.mesh"));

  doneOgreContext();

  // Connect the before rendering call to render now that we've finished initializing
  // Again the render call must be on the rendering thread.
  connect(this, &ExampleApp::beforeRendering, this, &ExampleApp::render, Qt::DirectConnection);

  emit(ogreInitialized());
}

void ExampleApp::render()
{
  activateOgreContext();
  Ogre::Root::getSingleton().renderOneFrame();
#ifdef _MSC_VER
  Ogre::Root::getSingleton()._updateAllRenderTargets();
#endif
  doneOgreContext();
}

void ExampleApp::timerEvent(QTimerEvent *)
{
  // This causes the beforeRendering signal to be activated
  update();
}

void ExampleApp::activateOgreContext()
{
  glPopAttrib();
  glPopClientAttrib();

  m_qtContext->functions()->glUseProgram(0);
  m_qtContext->doneCurrent();

  m_ogreContext->makeCurrent(this);
}

void ExampleApp::doneOgreContext()
{
  m_ogreContext->functions()->glBindBuffer(GL_ARRAY_BUFFER, 0);
  m_ogreContext->functions()->glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
  m_ogreContext->functions()->glBindRenderbuffer(GL_RENDERBUFFER, 0);
  m_ogreContext->functions()->glBindFramebuffer(GL_FRAMEBUFFER_EXT, 0);

  // unbind all possible remaining buffers; just to be on safe side
  m_ogreContext->functions()->glBindBuffer(GL_ARRAY_BUFFER, 0);
  m_ogreContext->functions()->glBindBuffer(GL_ATOMIC_COUNTER_BUFFER, 0);
  m_ogreContext->functions()->glBindBuffer(GL_COPY_READ_BUFFER, 0);
  m_ogreContext->functions()->glBindBuffer(GL_COPY_WRITE_BUFFER, 0);
  m_ogreContext->functions()->glBindBuffer(GL_DRAW_INDIRECT_BUFFER, 0);
  //    m_ogreContext->functions()->glBindBuffer(GL_DISPATCH_INDIRECT_BUFFER, 0);
  m_ogreContext->functions()->glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
  m_ogreContext->functions()->glBindBuffer(GL_PIXEL_PACK_BUFFER, 0);
  m_ogreContext->functions()->glBindBuffer(GL_PIXEL_UNPACK_BUFFER, 0);
  //    m_ogreContext->functions()->glBindBuffer(GL_SHADER_STORAGE_BUFFER, 0);
  m_ogreContext->functions()->glBindBuffer(GL_TEXTURE_BUFFER, 0);
  m_ogreContext->functions()->glBindBuffer(GL_TRANSFORM_FEEDBACK_BUFFER, 0);
  m_ogreContext->functions()->glBindBuffer(GL_UNIFORM_BUFFER, 0);

  m_ogreContext->doneCurrent();

  m_qtContext->makeCurrent(this);
  glPushAttrib(GL_ALL_ATTRIB_BITS);
  glPushClientAttrib(GL_CLIENT_ALL_ATTRIB_BITS);
}

void ExampleApp::addContent()
{
  // expose QML types
  qmlRegisterType<OgreItem>("Ogre", 1, 0, "OgreItem");

  // expose objects as QML globals
  rootContext()->setContextProperty("Window", this);

  // load the QML scene
  setResizeMode(QQuickView::SizeRootObjectToView);
  setSource(QUrl("qrc:/qml/example.qml"));
}
