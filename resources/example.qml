/*!
 * \copyright (c) Nokia Corporation and/or its subsidiary(-ies) (qt-info@nokia.com) and/or contributors
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * \license{This source file is part of QmlOgre abd subject to the BSD license that is bundled
 * with this source code in the file LICENSE.}
 */

import QtQuick 2.0
import Ogre 1.0

Rectangle {
    id: ogre
    width: 1024
    height: 768
    color: "black"

    Behavior on width { NumberAnimation { } }
    Behavior on height { NumberAnimation { } }

    Image {
        id: back
        anchors.fill: parent
        source: "qrc:/images/GrassandSky.png"
        Behavior on opacity { NumberAnimation { } }
    }


    OgreItemWrapper {
        id: ogreitemWrapper
    }

    CamController {
        id: camController
        cw: ogreitemWrapper.cw
    }


}
