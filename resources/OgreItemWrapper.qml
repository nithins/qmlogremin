import QtQuick 2.0
import Ogre 1.0

Rectangle {

    property alias cw : cw

    OgreItem {
        id: ogreitem
        width: 600; height: 400
        anchors.left: moveHideIcon.left
        anchors.leftMargin: -5
        anchors.top: moveHideIcon.bottom
        anchors.topMargin: 6

        Behavior on opacity { NumberAnimation { } }
        Behavior on width { NumberAnimation { } }
        Behavior on height { NumberAnimation { } }

        Item {
            id: cw
            property real yaw: 0
            property real pitch: 0
            property real zoom: 1

            onYawChanged: ogreitem.yaw = yaw
            onPitchChanged: ogreitem.pitch = pitch
            onZoomChanged: {zoom = Math.max(0.1,Math.min(zoom,6)); ogreitem.zoom = zoom;}
        }


        states: [
            State {
                name: "State1"

                PropertyChanges {
                    target: ogreitem
                    width: Window.width
                    height: Window.height
                }
                PropertyChanges {
                    target: moveHideIcon
                    x: 5
                    y: -moveHideIcon.height - 6
                }

                PropertyChanges {
                    target: aaIcon
                    anchors.top: ogreitem.top
                    anchors.topMargin: 5
                }
                PropertyChanges {
                    target: toolbar3
                    anchors.top: ogreitem.top
                    anchors.topMargin: 5
                }
                PropertyChanges {
                    target: back
                    opacity: 0
                }
            }

        ]

        MouseArea {
            anchors.fill: parent
            acceptedButtons: Qt.LeftButton | Qt.RightButton

            property int prevX: -1
            property int prevY: -1

            onPositionChanged: {
                if (pressedButtons & Qt.LeftButton) {
                    if (prevX > -1)
                        cw.yaw -= (mouse.x - prevX) / 4.0
                    if (prevY > -1)
                        cw.pitch -= (mouse.y - prevY) / 4.0
                    prevX = mouse.x
                    prevY = mouse.y
                }
                if (pressedButtons & Qt.RightButton) {
                    if (prevY > -1)
                        cw.zoom = cw.zoom - (mouse.y - prevY) / 100;
                    prevY = mouse.y
                }
            }
            onReleased: { prevX = -1; prevY = -1 }
        }
    }

    Rectangle {
        id: moveHideIcon
        x: 200
        y: 200
        width: 25
        height: 25
        radius: 5
        Behavior on x { NumberAnimation { } }
        Behavior on y { NumberAnimation { } }

        gradient: Gradient {
            GradientStop {
                position: 0
                color: "#c83e3e3e"
            }

            GradientStop {
                position: 1
                color: "#c8919191"
            }
        }

        border.width: 2
        border.color: "#1a1a1a"

        Image {
            anchors.rightMargin: 5
            anchors.leftMargin: 5
            anchors.bottomMargin: 5
            anchors.topMargin: 5
            anchors.fill: parent
            smooth: true
            fillMode: "Stretch"
            source: "qrc:/images/move.png"
        }

        MouseArea {
            anchors.fill: parent
            drag.target: moveHideIcon
            drag.axis: "XandYAxis"
            drag.minimumX: 0
            drag.minimumY: 0
            drag.maximumX: ogre.width - moveHideIcon.width
            drag.maximumY: ogre.height - moveHideIcon.height
            onClicked: ogreitem.opacity = ogreitem.opacity == 1 ? 0 : 1
        }
    }


    Rectangle {
        id: aaIcon
        width: 25
        height: 25
        radius: 5
        gradient: Gradient {
            GradientStop {
                position: 0
                color: "#c83e3e3e"
            }

            GradientStop {
                position: 1
                color: "#c8919191"
            }
        }
        anchors.top: moveHideIcon.top
        anchors.right: toolbar3.left
        anchors.rightMargin: 6
        border.color: "#1a1a1a"

        MouseArea {
            anchors.fill: parent
            onClicked: { ogreitem.smooth = !ogreitem.smooth }
        }

        Text {
            anchors.fill: parent
            text: "AA"
            font.bold: true
            font.pixelSize: parent.height * 0.55
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter

            Rectangle {
                height: parent.height
                width: 2
                anchors.centerIn: parent
                color: "#BB1111"
                rotation: 40
                visible: !ogreitem.smooth
            }
        }
        border.width: 2
    }

    Rectangle {
        id: toolbar3
        width: 25
        height: 25
        radius: 5
        gradient: Gradient {
            GradientStop {
                position: 0
                color: "#c83e3e3e"
            }

            GradientStop {
                position: 1
                color: "#c8919191"
            }
        }
        anchors.top: moveHideIcon.top
        anchors.right: ogreitem.right
        anchors.rightMargin: 5
        border.color: "#1a1a1a"

        MouseArea {
            anchors.fill: parent

            onClicked: {

                if(ogreitem.state === '' && Window.visibility !== 5)
                    ogreitem.state = 'State1'
                else if (ogreitem.state === 'State1' && Window.visibility !== 5)
                    Window.showFullScreen()
                else if (ogreitem.state === 'State1' && Window.visibility === 5)
                    ogreitem.state = '';
                else
                    Window.showNormal()
                }
        }

        Rectangle {
            id: toolbar31
            color: "#28ffffff"
            radius: 2
            border.width: 2
            border.color: "#000000"
            anchors.rightMargin: 7
            anchors.leftMargin: 7
            anchors.topMargin: 7
            anchors.bottomMargin: 7
            anchors.fill: parent

            Rectangle {
                id: toolbar311
                height: 3
                color: "#000000"
                anchors.right: parent.right
                anchors.left: parent.left
                anchors.top: parent.top
            }
        }
        border.width: 2
    }
}
