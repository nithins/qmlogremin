import QtQuick 2.0
import Ogre 1.0


Rectangle {

    property var cw

    Rectangle {
        id: rectangle1
        width: 139
        height: 208
        radius: 15
        gradient: Gradient {
            GradientStop {
                position: 0
                color: "#6f6f6f"
            }

            GradientStop {
                position: 0.24
                color: "#141414"
            }

            GradientStop {
                position: 1
                color: "#50000000"
            }
        }
        anchors.left: rectangle2.left
        anchors.leftMargin: -5
        anchors.top: rectangle2.bottom
        anchors.topMargin: 6
        border.width: 4
        border.color: "#1a1a1a"
        clip: false

        Behavior on opacity { PropertyAnimation { } }


        Button {
            anchors.horizontalCenterOffset: -35
            rotation: 90
            onButtonPressed: PropertyAnimation {
                target: cw; property: "yaw"; to: cw.yaw +20}
        }

        Button {
            anchors.horizontalCenterOffset: 35
            rotation: -90
            onButtonPressed: PropertyAnimation {
                target: cw; property: "yaw"; to: cw.yaw -20}
        }

        Button {
            anchors.verticalCenterOffset: -35
            rotation: 180
            onButtonPressed: PropertyAnimation {
                target: cw; property: "pitch"; to: cw.pitch+20}
        }

        Button {
            anchors.verticalCenterOffset: 35
            rotation: 0
            onButtonPressed: PropertyAnimation {
                target: cw; property: "pitch"; to: cw.pitch-20}
        }

        Button{
            anchors.verticalCenterOffset: -35
            anchors.horizontalCenterOffset: 35
            img_src: "qrc:/images/plus.png"
            onButtonPressed: PropertyAnimation {
                target: cw; property: "zoom"; to: cw.zoom+0.5}
        }
        Button{
            anchors.verticalCenterOffset: -35
            anchors.horizontalCenterOffset: -35
            img_src: "qrc:/images/minus.png"
            onButtonPressed: PropertyAnimation {
                target: cw; property: "zoom"; to: cw.zoom-0.5}
        }

        states: [
            State {
                name: "State1"

                PropertyChanges {
                    target: rectangle1
                    opacity: 0
                }
            }
        ]
    }

    Rectangle {
        id: rectangle2
        x: 31
        y: 269
        width: 25
        height: 25
        radius: 5
        gradient: Gradient {
            GradientStop {
                position: 0
                color: "#c83e3e3e"
            }

            GradientStop {
                position: 1
                color: "#c8919191"
            }
        }

        border.width: 2
        border.color: "#1a1a1a"

        Image {
            id: image2
            anchors.rightMargin: 5
            anchors.leftMargin: 5
            anchors.bottomMargin: 5
            anchors.topMargin: 5
            anchors.fill: parent
            smooth: true
            fillMode: "Stretch"
            source: "qrc:/images/move.png"
        }

        MouseArea {
            anchors.fill: parent
            drag.target: rectangle2
            drag.axis: "XandYAxis"
            drag.minimumX: 0
            drag.minimumY: 0
            drag.maximumX: ogre.width - rectangle2.width
            drag.maximumY: ogre.height - rectangle2.height
            onClicked: rectangle1.state = rectangle1.state == '' ? 'State1' : '';
        }
    }
}
