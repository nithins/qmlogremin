//contents of Button.qml
import QtQuick 2.0

Item {
    id: button
    width: 30
    height: 30
    anchors.centerIn: parent
    property alias rotation: image.rotation
    property alias img_src: image.source

    signal buttonPressed()
    signal buttonReleased()

    MouseArea {
        id: mousearea
        anchors.fill: parent
        onPressed: buttonPressed()
        onReleased: buttonReleased()
    }

    Image {
        id: image
        smooth: true
        anchors.fill: parent
        source: "qrc:/images/arrow.png"
    }
}

