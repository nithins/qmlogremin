/*!
 * \copyright (c) Nokia Corporation and/or its subsidiary(-ies) (qt-info@nokia.com) and/or contributors
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * \license{This source file is part of QmlOgre abd subject to the BSD license that is bundled
 * with this source code in the file LICENSE.}
 */


#include <RenderSystems/GL/OgreGLTexture.h>
#include <RenderSystems/GL/OgreGLFrameBufferObject.h>
#include <RenderSystems/GL/OgreGLFBORenderTexture.h>

#include <QOpenGLContext>
#include <QOpenGLFunctions>
#include <QSGGeometryNode>


#include "ogreitem.h"

OgreItem::OgreItem(QQuickItem *parent)
  : QQuickItem(parent)
  , m_geometry(QSGGeometry::defaultAttributes_TexturedPoint2D(), 4)
  , m_texture(0)
  , m_renderTarget(0)
  , m_camera(0)
  , m_camera_node(0)
  , m_yaw(0)
  , m_pitch(0)
  , m_zoom(1)
{
  setFlag(ItemHasContents);
  setSmooth(false);

  Ogre::Root::getSingleton().addFrameListener(this);
}

OgreItem::~OgreItem()
{
}

QSGNode *OgreItem::updatePaintNode(QSGNode *oldNode, UpdatePaintNodeData *)
{
	connect(window(), &QQuickWindow::beforeSynchronizing, this, &QQuickItem::update);

  if (width() <= 0 || height() <= 0 || !m_texture)
  {
    if(oldNode)
      delete oldNode;
    return 0;
  }

  QSGGeometryNode *node = static_cast<QSGGeometryNode *>(oldNode);

  if(!node)
  {
    node = new QSGGeometryNode();
    node->setGeometry(&m_geometry);
    node->setMaterial(&m_material);
    node->setOpaqueMaterial(&m_materialO);
  }

  node->markDirty(QSGNode::DirtyGeometry);
  node->markDirty(QSGNode::DirtyMaterial);
  return node;
}

bool OgreItem::frameStarted(const Ogre::FrameEvent& evt)
{
  updateCamera();
  updateFBO();
  return true;
}

void OgreItem::updateCamera()
{
  static const Ogre::Vector3 initialPosition(0, 0, 300);

  if(!m_camera)
  {
    Ogre::SceneManager *sceneManager = Ogre::Root::getSingleton().getSceneManager("mySceneManager");

    Ogre::Camera *camera = sceneManager->createCamera("myCamera");
    camera->setNearClipDistance(1);
    camera->setFarClipDistance(99999);
    camera->setAspectRatio(1);
    camera->setAutoTracking(true, sceneManager->getRootSceneNode());

    m_camera = camera;
    m_camera_node = sceneManager->getRootSceneNode()->createChildSceneNode();
    m_camera_node->attachObject(camera);
    camera->move(initialPosition);

  }

  m_camera_node->resetOrientation();
  m_camera->setPosition(initialPosition * (1 / m_zoom));
  m_camera_node->yaw(Ogre::Radian(Ogre::Degree(m_yaw)));
  m_camera_node->pitch(Ogre::Radian(Ogre::Degree(m_pitch)));
}

void OgreItem::updateFBO()
{
  QSize wsz(width(),height());

  if (width() <= 0 || height() <= 0 || (wsz == m_size) || !m_camera)
    return;

  m_size = wsz;

  if (m_renderTarget)
    Ogre::TextureManager::getSingleton().remove("RttTex");


  Ogre::TexturePtr rtt = Ogre::TextureManager::getSingleton().createManual
      ("RttTex",Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME,
       Ogre::TEX_TYPE_2D,m_size.width(),m_size.height(),0,
       Ogre::PF_R8G8B8A8,Ogre::TU_RENDERTARGET, 0, false);

  m_renderTarget = rtt->getBuffer()->getRenderTarget();

  m_renderTarget->addViewport(m_camera);
  m_renderTarget->getViewport(0)->setClearEveryFrame(true);
  m_renderTarget->getViewport(0)->setBackgroundColour(Ogre::ColourValue::Black);
  m_renderTarget->getViewport(0)->setOverlaysEnabled(false);
  m_renderTarget->addListener(this);

  Ogre::Real aspectRatio = Ogre::Real(m_size.width()) / Ogre::Real(m_size.height());
  m_camera->setAspectRatio(aspectRatio);

  QSGGeometry::updateTexturedRectGeometry(&m_geometry,
                                          QRectF(0, 0, m_size.width(), m_size.height()),
                                          QRectF(0, 0, 1, 1));

  Ogre::GLTexture *nativeTexture = static_cast<Ogre::GLTexture *>(rtt.get());

  delete m_texture;

  m_texture = window()->createTextureFromId(nativeTexture->getGLID(),m_size);

  m_material.setTexture(m_texture);
  m_materialO.setTexture(m_texture);

  return;
}

void OgreItem::preRenderTargetUpdate(const Ogre::RenderTargetEvent& evt)
{
  if (m_renderTarget)
    Ogre::GLFBOManager::getSingleton().bind(m_renderTarget);
}

void OgreItem::postRenderTargetUpdate(const Ogre::RenderTargetEvent& evt)
{
  if (m_renderTarget)
    Ogre::GLFBOManager::getSingleton().unbind(m_renderTarget);
}

